/*
 * Fichero: pulsador.h
 *
 * Grupo 4	Autor Daniel Franco
 *
 *		v1.0	Fecha: 2014-III-18
 *
 *              v1.1    Fecha: Abril-2014
 */

#include "p24hj256gp610A.h"

//Inicializa los pulsadores
void Inic_Pulsadores(void);

// Gestiona los pulsadores por interrupción
void _ISR _CNInterrupt(void);