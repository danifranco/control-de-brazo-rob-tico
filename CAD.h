/*
 * Fichero: CAD.h
 *
 * Grupo 4	Autor Daniel Franco
 *
 *		v1.0	Fecha: 2014-III-18
 *
 *              v1.1    Fecha: Mayo-2014
 */

#include "p24HJ256GP610A.h"
#include "pulsador.h"
#include "LCD.h"
#include "PWM.h"

#define potenciometro _TRISB5
#define sensor _TRISB4
#define palanca _TRISB8
#define J1PX _TRISB9
#define J1PY _TRISB10
#define incX _TRISB11
#define incY _TRISB12
#define incZ _TRISB13

//Define el timer a utilizar
#define cad_timer 9

//Variables extendidas 
extern int joystick;
extern unsigned char Ventana_LCD[2][16];

//Inicializa el modulo CA/D
void inicializarCAD();

//Reestablece los valores del joystick para que el brazo
//se ponga en una posición normal antes de empezar a moverlo
void posicionarBrazo();

//Activamos las interrupciones de los timers del CAD
void activarTimersCAD();

//Desactiva los timer del CAD
void descativarTimersCAD();

//Interrupción del timerx
//Controlará los valores del sensor y del potenciometro
void _ISR _T_CAD_Interrupt(void);

#if (cad_timer == 1)
    #define _T_CAD_Interrupt _T1Interrupt
    #define _TcadIF _T1IF
    #define _TcadIE _T1IE
#elif (cad_timer == 2)
    #define _T_CAD_Interrupt _T2Interrupt
    #define _TcadIF _T2IF
    #define _TcadIE _T2IE
#elif (cad_timer == 3)
    #define _T_CAD_Interrupt _T3Interrupt
    #define _TcadIF _T3IF
    #define _TcadIE _T3IE
#elif (cad_timer == 4)
    #define _T_CAD_Interrupt _T4Interrupt
    #define _TcadIF _T4IF
    #define _TcadIE _T4IE
#elif (cad_timer == 5)
    #define _T_CAD_Interrupt _T5Interrupt
    #define _TcadIF _T5IF
    #define _TcadIE _T5IE
#elif (cad_timer == 6)
    #define _T_CAD_Interrupt _T6Interrupt
    #define _TcadIF _T6IF
    #define _TcadIE _T6IE
#elif (cad_timer == 7)
    #define _T_CAD_Interrupt _T7Interrupt
    #define _TcadIF _T7IF
    #define _TcadIE _T7IE
#elif (cad_timer == 8)
    #define _T_CAD_Interrupt _T8Interrupt
    #define _TcadIF _T8IF
    #define _TcadIE _T8IE
#elif (cad_timer == 9)
    #define _T_CAD_Interrupt _T9Interrupt
    #define _TcadIF _T9IF
    #define _TcadIE _T9IE
#elif (cad_timer == 0 || cad_timer > 9)
    #error ("TIMER para CA/D NO DEFINIDO")
#endif