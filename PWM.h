/*
 * Fichero: PWM.h
 *
 * Grupo 4	Autor Daniel Franco
 *
 *		v1.0	Fecha: 2014-III-18
 *
 *              v1.1    Fecha: Mayo-2014
 */

#include "p24HJ256GP610A.h"
#include "RS232_2.h"

#define MIN_POT_EJEX 150
#define MAX_POT_EJEX 1000

#define MIN_POT_EJEY 60
#define MAX_POT_EJEY 950

#define MAX_PALANCA 1000
#define MIN_PALANCA 80

#define MIN_SERVO 350
#define MAX_SERVO 1550
#define mitad_servo ((MAX_SERVO - MIN_SERVO)/2 + MIN_SERVO)

#define MIN_INCLINOMETRO_Y 350
#define MAX_INCLINOMETRO_Y 600
#define mitad_inc_Y ((MAX_INCLINOMETRO_Y - MIN_INCLINOMETRO_Y)/2 + MIN_INCLINOMETRO_Y)
//Relación entre los valores posibles del servo y los del inclinómetro del eje Y
#define relacion_servo_inc (float) (MAX_SERVO - MIN_SERVO)/(MAX_INCLINOMETRO_Y - MIN_INCLINOMETRO_Y)

//Define el timer a utilizar
#define pwm_timer 2
#define pwm_timer_soft 3

#define TRIS_pwm1 _TRISD0
#define TRIS_pwm2 _TRISD1
#define TRIS_pwm3 _TRISD2
#define TRIS_pwm4 _TRISD3

#define TRIS_pwm5_soft _TRISF7
#define LAT_pwm5_soft _LATF7

//Lo utilizaremos para calcular el ratio del eje X
#define ratioX (float) (MAX_SERVO - MIN_SERVO) / (MAX_POT_EJEX - MIN_POT_EJEX)

//Lo utilizaremos para calcular el ratio del eje Y
#define ratioY (float) (MAX_SERVO - MIN_SERVO) / (MAX_POT_EJEY - MIN_POT_EJEY)

//Lo utilizaremos para calcular el ratio de la palanca
#define ratioPalanca (float) (MAX_SERVO - MIN_SERVO) / (MAX_PALANCA - MIN_PALANCA)

//Inicialización del modulo de Salida Comparada(PWM)
void inicializar_PWM();

//Interrupción del timer asociado al PWM
void _ISR _T_PWM_Interrupt( void );

//Interrupcion del timer asociado al PWM
void _ISR _T_PWM_soft_Interrupt( void );
    
#if (pwm_timer == 2)
    #define _T_PWM_Interrupt _T2Interrupt
    #define _TpwmIF _T2IF
    #define _TpwmIE _T2IE
#elif (pwm_timer == 3)
    #define _T_PWM_Interrupt _T3Interrupt
    #define _TpwmIF _T3IF
    #define _TpwmIE _T3IE
#elif (pwm_timer < 2 || pwm_timer > 3)
    #error ("TIMER para PWM NO DEFINIDO")
#endif

#if (pwm_timer_soft == 2)
    #define _T_PWM_soft_Interrupt _T2Interrupt
    #define _Tpwm_softIF _T2IF
    #define _Tpwm_softIE _T2IE
    #define _Tpwm_PR PR2
    #define _Tpwm_TMR TMR3
#elif (pwm_timer_soft == 3)
    #define _T_PWM_soft_Interrupt _T3Interrupt
    #define _Tpwm_softIF _T3IF
    #define _Tpwm_softIE _T3IE
    #define _Tpwm_PR PR3
    #define _Tpwm_TMR TMR3
#elif (pwm_timer_soft < 2 || pwm_timer_soft > 3)
    #error ("TIMER para PWM NO DEFINIDO")
#endif