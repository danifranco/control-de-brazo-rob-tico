/*
 * Fichero: oscilador.c
 *
 * Grupo 4	Autor Daniel Franco
 *
 *		v1.0	Fecha: 2014-III-18
 *
 *              v1.1    Fecha: Abril-2014
 */

#include "Oscilador.h"

_FOSCSEL (FNOSC_PRIPLL);	// Primary oscillator (XT, HS, EC) w/ PPL
_FOSC (FCKSM_CSECMD & OSCIOFNC_OFF & POSCMD_XT);
                                    // Clock switching and clock monitor: Both
												// disable, OSC2 is clock O/P,XT oscillator
_FWDT (FWDTEN_OFF);		// Watchdog Timer:Disabled

//Inicializacion del oscilador
void Inic_Oscilator (void)
{
// Configure Oscillator to operate the device at 40MHz
// Fosc= Fin*M(N1*N2),  Fcy=Fosc/2
// Fosc= 8M*40/(2*2)= 80MHz for 8M input clock
// M=40 --> 80 MHz Instruccion 25ns
// M=20 --> 40 MHz Instruccion 50ns
// M=10 --> 20 MHz Instruccion 100ns

	PLLFBD = 40-2;
	CLKDIVbits.PLLPOST = 0;
	CLKDIVbits.PLLPRE = 0;
//	OSCTUN = 0;		// TUNE FRC oscillator, if FRC is used

	RCONbits.SWDTEN = 0;	// Disable Watch Dog Timer

// Funciones pre-compiladas: MPLAB C30 -DS51284G- apendice B
// Clock switch to incorporate PLL

	__builtin_write_OSCCONH(0x03);	// Initiate Clock Switch to Primary
					// Oscillator with PLL (NOSC=0b011)
	__builtin_write_OSCCONL(0x01);	// Start clock switching
	while (OSCCONbits.COSC != 0b011);// Wait fot Clock switch to occur

// Wait for PLL to lock
	while(OSCCONbits.LOCK !=1) {}
} // Fin Inic_Oscilator

