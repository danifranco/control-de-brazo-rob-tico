/*
 * Fichero: LED.h
 *
 * Grupo 4	Autor Daniel Franco
 *
 *		v1.0	Fecha: 2014-III-18
 *
 *              v1.1    Fecha: Abril-2014
 */

#include "p24hj256gp610A.h"
#include "pulsador.h"

//Define el timer a utilizar
#define luces_timer 6

//Variables extendidas de control de estado principal. Se utilizarán
//para resetearlas una vez volvamos al menu principal
extern int pulsado;
extern int salir;
extern int dentro;

//Inicializa los leds y su timer asociado
void Inic_Leds();

//Funcion que mostrará las luces intermitentes
void lucesIntermitentes();

#if (luces_timer == 1)
    #define _TlucesIF _T1IF
#elif (luces_timer == 2)
    #define _TlucesIF _T2IF
#elif (luces_timer == 3)
    #define _TlucesIF _T3IF
#elif (luces_timer == 4)
    #define _TlucesIF _T4IF
#elif (luces_timer == 5)
    #define _TlucesIF _T5IF
#elif (luces_timer == 6)
    #define _TlucesIF _T6IF
#elif (luces_timer == 7)
    #define _TlucesIF _T7IF
#elif (luces_timer == 8)
    #define _TlucesIF _T8IF
#elif (luces_timer == 9)
    #define _TlucesIF _T9IF
#elif (luces_timer == 0 || luces_timer > 9)
    #error ("TIMER para LUCES NO DEFINIDO")
#endif