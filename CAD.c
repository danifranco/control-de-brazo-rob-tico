/*
 * Fichero: CAD.c
 *
 * Grupo 4	Autor Daniel Franco
 *
 *		v1.0	Fecha: 2014-III-18
 *
 *      v1.1    Fecha: Mayo-2014
 */

#include "CAD.h"

//Variables del potenciometro y sensor
int val1,val2,val3,val4; //Variables auxiliares para guardar cada digito del potenciometro
int estadoCAD; //Control de la m�quina de estados del CAD
unsigned int ADCValue; //Variable en la que se guardar� el valor del potenciometro
int servoX,servoY,valorPalanca; //Valores de los potenciometros que se utilizar�n para mover el brazo

extern int inclinometroX,inclinometroY, inclinometroZ; //Valores de los ejes del inclin�metro

//Inicializa el CA/D
void inicializarCAD(){
    potenciometro=1;
    sensor=1;
    palanca=1;
    J1PX=1;
    J1PY=1;
    servoX=400;
    servoY=400;
    valorPalanca=400;
    estadoCAD=0;
    incX=1;
    incY=1;
    incZ=1;

    AD1PCFGL = 0xC0CF; // Las patas del puerto B digitales, menos RB5,RB4,RB8,RB9,RB10,RB11,RB12,RB13 anal�gicas
    AD1CON1 = 0x00E0; // SSRC bit = 111 auto-convert
    AD1CSSL = 0;
    AD1CON3 = 0x1F02; // Tiempo de muestreo = 31Tad, Tad interno = 2 Tcy
    AD1CON2 = 0;
    AD1CON1bits.ADON = 1; // Encendido del m�dulo
    AD1CON1bits.SAMP = 1; // Comienza el muestreo y al de 12Tad empieza la conversi�n

    //Inicializacion timerx que controlar� los valores
    //del potenciometro y del sensor
    #if(cad_timer == 1)
    _T1IF=0;
    _T1IE=0;
    T1CON=0;
    PR1= 40000;
    T1CONbits.TON=1;
    #endif
    //=============================================
    #if(cad_timer == 2)
    _T2IF=0;
    _T2IE=0;
    T2CON=0;
    PR2= 40000;
    T2CONbits.TON=1;
    #endif
    //=============================================
    #if(cad_timer == 3)
    _T3IF=0;
    _T3IE=0;
    T3CON=0;
    PR3= 40000;
    T3CONbits.TON=1;
    #endif
    //=============================================
    #if(cad_timer == 4)
    _T4IF=0;
    _T4IE=0;
    T4CON=0;
    PR4= 40000;
    T4CONbits.TON=1;
    #endif
    //=============================================
    #if(cad_timer == 5)
    _T5IF=0;
    _T5IE=0;
    T5CON=0;
    PR5= 40000;
    T5CONbits.TON=1;
    #endif
    //=============================================
    #if(cad_timer == 6)
    _T6IF=0;
    _T6IE=0;
    T6CON=0;
    PR6= 40000;
    T6CONbits.TON=1;
    #endif
    //=============================================
    #if(cad_timer == 7)
    _T7IF=0;
    _T7IE=0;
    T7CON=0;
    PR7= 40000;
    T7CONbits.TON=1;
    #endif
    #if(cad_timer == 8)
    _T8IF=0;
    _T8IE=0;
    T8CON=0;
    PR8= 40000;
    T8CONbits.TON=1;
    #endif
    //=============================================
    #if(cad_timer == 9)
    _T9IF=0;
    _T9IE=0;
    T9CON=0;
    PR9= 40000;
    T9CONbits.TON=1;
    #endif
}

//Reestablece los valores del joystick para que el brazo 
//se ponga en una posici�n normal antes de empezar a moverlo
void posicionarBrazo() {
    servoX=400;
    servoY=400;
    valorPalanca=400;
}

//Activamos las interrupciones de los timers del CAD
void activarTimersCAD(){
    _TcadIE=1;
    _TlcdIE=1;
    //Activamos el timer del servo
    _TpwmIE=1;
}

//Desactiva los timer del CAD
void descativarTimersCAD(){
    _TcadIE=0;
    _TlcdIE=0;
    //Desactivamos el timer del servo
    _TpwmIE=0;
}

//Interrupci�n del timer x
//Controlar� los valores del sensor y del potenciometro
void _ISR _T_CAD_Interrupt(void){
    if (joystick == 2 && estadoCAD==0)estadoCAD=2;
    else if(joystick == 0 || joystick == 1){
        estadoCAD=0;
        joystick= 3;
    }
    switch(estadoCAD){
        //Primer estado del CAD que cogeremos los valores del potenciometro
        case 0:
            //Ponemos el canal del potenciometro
            AD1CHS0 = 0x0005;
            ADCValue = ADC1BUF0;

            val1 = (int)ADCValue/1000;
            Ventana_LCD[1][6]=val1+0x30;

            val2 = (int)(ADCValue -val1*1000)/100;
            Ventana_LCD[1][7]=val2+0x30;

            val3 = (int)(ADCValue -val1*1000 -val2*100)/10;
            Ventana_LCD[1][8]=val3+0x30;

            val4 = (int) (ADCValue -val1*1000 -val2*100 -val3*10);
            Ventana_LCD[1][9]=val4+0x30;
            estadoCAD=1;
        break;
        //Segundo estado del CAD que cogeremos los valores del sensor de temperatura
        case 1:
            //Ponemos el canal del sensor de temperatura
            AD1CHS0 = 0x0004;
            ADCValue = ADC1BUF0;

            val1 = (int)ADCValue/1000;
            Ventana_LCD[0][7]=val1+0x30;

            val2 = (int)(ADCValue -val1*1000)/100;
            Ventana_LCD[0][8]=val2+0x30;

            val3 = (int)(ADCValue -val1*1000 -val2*100)/10;
            Ventana_LCD[0][9]=val3+0x30;

            val4 = (int) (ADCValue -val1*1000 -val2*100 -val3*10);
            Ventana_LCD[0][10]=val4+0x30;
            estadoCAD=0;
        break;

        //Tercer estado del CAD en el que cogeremos los valores de la palanca
        case 2:
            AD1CHS0 = 0x0008;
            ADCValue = ADC1BUF0;

            //Cargamos el valor del eje Z del inclin�metro
            inclinometroZ=ADCValue;

            val1 = (int)ADCValue/1000;
            Ventana_LCD[1][12]=val1+0x30;

            val2 = (int)(ADCValue -val1*1000)/100;
            Ventana_LCD[1][13]=val2+0x30;

            val3 = (int)(ADCValue -val1*1000 -val2*100)/10;
            Ventana_LCD[1][14]=val3+0x30;

            val4 = (int) (ADCValue -val1*1000 -val2*100 -val3*10);
            Ventana_LCD[1][15]=val4+0x30;
            estadoCAD=3;
        break;

        //Cuarto estado del CAD en el que cogeremos los valores
        //del primer joystick del eje X 
        case 3:
            AD1CHS0 = 0x0009;
            ADCValue = ADC1BUF0;

            //Cargamos el valor de la palanca del joystick
            valorPalanca=ADCValue;

            val1 = (int)ADCValue/1000;
            Ventana_LCD[1][0]=val1+0x30;

            val2 = (int)(ADCValue -val1*1000)/100;
            Ventana_LCD[1][1]=val2+0x30;

            val3 = (int)(ADCValue -val1*1000 -val2*100)/10;
            Ventana_LCD[1][2]=val3+0x30;

            val4 = (int) (ADCValue -val1*1000 -val2*100 -val3*10);
            Ventana_LCD[1][3]=val4+0x30;
            estadoCAD=4;
        break;

        //Quinto estado del CAD en el que cogeremos los valores
        //del primer joystick del eje Y 
        case 4:
            AD1CHS0 = 0x000A;
            ADCValue = ADC1BUF0;

            //Cargamos el valor del eje X del joystick
            servoX= ADCValue;

            val1 = (int)ADCValue/1000;
            Ventana_LCD[1][6]=val1+0x30;

            val2 = (int)(ADCValue -val1*1000)/100;
            Ventana_LCD[1][7]=val2+0x30;

            val3 = (int)(ADCValue -val1*1000 -val2*100)/10;
            Ventana_LCD[1][8]=val3+0x30;

            val4 = (int) (ADCValue -val1*1000 -val2*100 -val3*10);
            Ventana_LCD[1][9]=val4+0x30;
            estadoCAD=5;
        break;
		
        //Sexto estado del CAD en el que cogeremos los valores
        //del inclin�metro del eje X
        case 5:
            AD1CHS0 = 0x000C;
            ADCValue = ADC1BUF0;
            //Cargamos el valor del eje Y del joystick
            servoY=ADCValue;

            estadoCAD=6;
        break;
		
        //S�ptimo estado del CAD en el que cogeremos los valores
        //del inclin�metro del eje Y
        case 6:
            AD1CHS0 = 0x000B;
            ADCValue = ADC1BUF0;	
            //Cargamos el valor del eje X del inclin�metro
            inclinometroX=ADCValue;
			
            estadoCAD=7;
        break;
		
        //Octavo estado del CAD en el que cogeremos los valores
        //del inclin�metro del eje Z
        case 7:
            AD1CHS0 = 0x000D;
            ADCValue = ADC1BUF0;      
            //Cargamos el valor del eje Y del inclin�metro
            inclinometroY=ADCValue;
            estadoCAD=2;
          
        break;
    }
    AD1CON1bits.SAMP = 1;
    
    _TcadIF = 0;
}

