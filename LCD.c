/*
 * Fichero: LCD.c
 *
 * Grupo 4	Autor Daniel Franco
 *
 *		v1.0	Fecha: 2014-III-18
 *
 *              v1.1    Fecha: Abril-2014
 */

#include "LCD.h"

//Varibles de control para la escritura en el LCD
unsigned char Ventana_LCD[2][16]; //Reserva 2x16 bytes
int estado;
int indice;

//========= Env�o de COMANDO =========
void lcd_cmd( char cmd ) // subrutina para comandos
{
 // TRISD &= 0xFF00; // asegura RD0 - RD7 como salidas
 DATA &= 0xFF00; // prepara RD0 - RD7
 DATA |= cmd; // command del lcd
 RW = 0; // Orden de escritura RW -> 0
 RS = 0; // Selecciona registro ?0?
 E = 1; // Activa la se�al E
 
 // Tiempo de espera requerido
 Nop();
 Nop();
 Nop();
 Nop();
 Nop();
 Nop();
 Nop();
 Nop();
 Nop();
 Nop();

 // Final de escritura
 E = 0; 
}


//======= Envio de DATO ===============
void lcd_data( char data ) // subroutina para datos
{
// TRISD &= 0xFF00; // asegura RD0 - RD7 como salidas
 RW = 0; // Orden de escritura RW -> 0
 RS = 1; // Selecciona registro ?1?
 DATA &= 0xFF00; // prepara RD0 - RD7
 DATA |= data; // dato del lcd
 E = 1; // Activa la se�al E

 // Tiempo de espera requerido
 Nop();
 Nop();
 Nop();
 Nop();
 Nop();
 Nop();
 Nop();
 Nop();
 Nop();
 Nop();

 // Final de escritura
 E = 0;
}

//Copia el mensaje correspondiente a la ventana_LCD dependiendo
//del n�mero pasado por par�metro
void copia_FLASH_RAM(int decision){
    //MENSAJE DEL MENU PRINCIPAL
    const char Texto_LCD_1[]= {" S3    S6    S4 " // 16 caracteres Fila 1
    " LUZ  CA/D  CRON"}; // 16 caracteres Fila 2

    //MENSAJE DEL CRONOMETRO
    const char Texto_LCD_2[]= {"---- PIC24H ----" // 16 caracteres Fila 1
    "Crono: --/--/-- "}; // 16 caracteres Fila 2

    //MENSAJE DE LAS LUCES
    const char Texto_LCD_3[]= {"---- LUCES ---- " // 16 caracteres Fila 1
    "  S6 <--- MENU  "}; // 16 caracteres Fila 2

    //MENSAJE DEL POTENCIOMETRO/SENSOR
    const char Texto_LCD_4[]= {"Poten: ----     " // 16 caracteres Fila 1
    "Temp: ----      "}; // 16 caracteres Fila 2

    //MENSAJE DEL JOYSTICK
    const char Texto_LCD_5[]= {"Palan. J1PX J1PY" // 16 caracteres Fila 1
    "----  ----  ----"}; // 16 caracteres Fila 2
    
    //MENSAJE DEL MENU DEL CAD
    const char Texto_LCD_6[]= {" S3        S4   " // 16 caracteres Fila 1
    "Joys.   Pote/Sen"}; // 16 caracteres Fila 2
    int i,j;

    switch(decision){
        //Copiar el mensaje del menu principal
        case 1:
            for(i=0; i<16;i++){
            Ventana_LCD [0][i]=Texto_LCD_1[i];
            }
            i=16;
            for(j=0; j<16;i++,j++){
                Ventana_LCD [1][j]=Texto_LCD_1[i];
            }
            break;

        //Copiar el mensaje del cronometro
        case 2:  
            for(i=0; i<16;i++){
                Ventana_LCD [0][i]=Texto_LCD_2[i];
            }
            i=16;
            for(j=0; j<16;i++,j++){
                Ventana_LCD [1][j]=Texto_LCD_2[i];
            }
            break;
        //Copiar el mensaje de las luces
        case 3:
            for(i=0; i<16;i++){
            Ventana_LCD [0][i]=Texto_LCD_3[i];
            }
            i=16;
            for(j=0; j<16;i++,j++){
                Ventana_LCD [1][j]=Texto_LCD_3[i];
            }
            break;
        //Copia el mensaje del potenciometro/sensor
        case 4:
            for(i=0; i<16;i++){
            Ventana_LCD [0][i]=Texto_LCD_4[i];
            }
            i=16;
            for(j=0; j<16;i++,j++){
                Ventana_LCD [1][j]=Texto_LCD_4[i];
            }
            break;
        //Copia el mensaje del joystick
        case 5:
            for(i=0; i<16;i++){
            Ventana_LCD [0][i]=Texto_LCD_5[i];
            }
            i=16;
            for(j=0; j<16;i++,j++){
                Ventana_LCD [1][j]=Texto_LCD_5[i];
            }
            break;
        //Copia el mensaje del menu del CAD
        case 6:
            for(i=0; i<16;i++){
            Ventana_LCD [0][i]=Texto_LCD_6[i];
            }
            i=16;
            for(j=0; j<16;i++,j++){
                Ventana_LCD [1][j]=Texto_LCD_6[i];
            }
            break;        
    } 
}

//Inicializaci�n del LCD
void inicializar_lcd(void){
    RW=0;
    RS=0;
    E=0;
    RW_TRIS =0;
    RS_TRIS =0;
    E_TRIS =0;
    TRISE =0xFF00;
	estado=1;
	indice=0;
	
    /* Inicializaci�n del timer que enviar� los caracteres al LCD*/
    //=============================================
    #if(lcd_timer == 1)
    _T1IF=0;
    _T1IE=0;
    T1CON=0;
    T1CONbits.TON=1;
    PR1= 52000;
    #endif
    //=============================================
    #if(lcd_timer == 2)
    _T2IF=0;
    _T2IE=0;
    T2CON=0;
    T2CONbits.TON=1;
    PR2= 52000;
    #endif
    //=============================================
    #if(lcd_timer == 3)
    _T3IF=0;
    _T3IE=0;
    T3CON=0;
    T3CONbits.TON=1;
    PR3= 52000;
    #endif
    //=============================================
    #if(lcd_timer == 4)
    _T4IF=0;
    _T4IE=0;
    T4CON=0;
    T4CONbits.TON=1;
    PR4= 52000;
    #endif
    //=============================================
    #if(lcd_timer == 5)
    _T5IF=0;
    _T5IE=0;
    T5CON=0;
    T5CONbits.TON=1;
    PR5= 52000;
    #endif
    //=============================================
    #if(lcd_timer == 6)
    _T6IF=0;
    _T6IE=0;
    T6CON=0;
    T6CONbits.TON=1;
    PR6= 52000;
    #endif
    //=============================================
    #if(lcd_timer == 7)
    _T7IF=0;
    _T7IE=0;
    T7CON=0;
    T7CONbits.TON=1;
    PR7= 52000;
    #endif
    //=============================================
    #if(lcd_timer == 8)
    _T8IF=0;
    _T8IE=0;
    T8CON=0;
    T8CONbits.TON=1;
    PR8= 52000;
    #endif
    //=============================================
    #if(lcd_timer == 9)
    _T9IF=0;
    _T9IE=0;
    T9CON=0;
    T9CONbits.TON=1;
    PR9= 52000;
    #endif



    /* Inicializaci�n del timer que realizar�
     * la espera entre caracteres*/
    //=============================================
    #if(lcd_timer_flag == 1)
    _T1IF=0;
    _T1IE=0;
    T1CON=0;
    T1CONbits.TON=1;

    //prescaler a 1:64
    T1CONbits.TCKPS0=0;
    T1CONbits.TCKPS1=1;
    #endif
    //=============================================
    #if(lcd_timer_flag == 2)
    _T2IF=0;
    _T2IE=0;
    T2CON=0;
    T2CONbits.TON=1;

    //prescaler a 1:64
    T2CONbits.TCKPS0=0;
    T2CONbits.TCKPS1=1;
    #endif
    //=============================================
    #if(lcd_timer_flag == 3)
    _T3IF=0;
    _T3IE=0;
    T3CON=0;
    T3CONbits.TON=1;

    //prescaler a 1:64
    T3CONbits.TCKPS0=0;
    T3CONbits.TCKPS1=1;
    #endif
    //=============================================
    #if(lcd_timer_flag == 4)
    _T4IF=0;
    _T4IE=0;
    T4CON=0;
    T4CONbits.TON=1;

    //prescaler a 1:64
    T4CONbits.TCKPS0=0;
    T4CONbits.TCKPS1=1;
    #endif
    //=============================================
    #if(lcd_timer_flag == 5)
    _T5IF=0;
    _T5IE=0;
    T5CON=0;
    T5CONbits.TON=1;

    //prescaler a 1:64
    T5CONbits.TCKPS0=0;
    T5CONbits.TCKPS1=1;
    #endif
    //=============================================
    #if(lcd_timer_flag == 6)
    _T6IF=0;
    _T6IE=0;
    T6CON=0;
    T6CONbits.TON=1;

    //prescaler a 1:64
    T6CONbits.TCKPS0=0;
    T6CONbits.TCKPS1=1;
    #endif
    //=============================================
    #if(lcd_timer_flag == 7)
    _T7IF=0;
    _T7IE=0;
    T7CON=0;
    T7CONbits.TON=1;

    //prescaler a 1:64
    T7CONbits.TCKPS0=0;
    T7CONbits.TCKPS1=1;
    #endif
    //=============================================
    #if(lcd_timer_flag == 8)
    _T8IF=0;
    _T8IE=0;
    T8CON=0;
    T8CONbits.TON=1;

    //prescaler a 1:64
    T8CONbits.TCKPS0=0;
    T8CONbits.TCKPS1=1;
    #endif
    //=============================================
    #if(lcd_timer_flag == 9)
    _T9IF=0;
    _T9IE=0;
    T9CON=0;
    T9CONbits.TON=1;

    //prescaler a 1:64
    T9CONbits.TCKPS0=0;
    T9CONbits.TCKPS1=1;
    #endif

    // Espera de 15ms
    // 15ms/1600ns = 9375 ciclos. El ciclo del reloj es de 1600ns porque hemos utilizado el prescaler (1:64)
    PR_Timer_Flag=9375;
    while(_Tlcd_flagIF == 0){ }
    lcd_cmd(0x38);
    _Tlcd_flagIF =0;

    // Espera de 5ms. El ciclo del reloj es de 1600ns porque hemos utilizado el prescaler (1:64)
    // 15ms/1600ns = 3125 ciclos
    PR_Timer_Flag=3125;
    while(_Tlcd_flagIF == 0){ }
    lcd_cmd(0x38);
    _Tlcd_flagIF =0;

    // Espera de 40 microsegundos. El ciclo del reloj es de 1600ns porque hemos utilizado el prescaler (1:64)
    // 40000/1600= 25 ciclos
    PR_Timer_Flag=25;
    while(_Tlcd_flagIF == 0){ }
    lcd_cmd(0x38);
    _Tlcd_flagIF =0;

    while(_Tlcd_flagIF == 0){ }
    lcd_cmd(0x38);
    _Tlcd_flagIF =0;

    //Encendemos el Display
    while(_Tlcd_flagIF == 0){ }
    lcd_cmd(0x0C);
    _Tlcd_flagIF =0;

    while(_Tlcd_flagIF == 0){ }
    lcd_cmd(0x06);
    _Tlcd_flagIF =0;

    //�ltima espera
    while(_Tlcd_flagIF == 0){ }
    _Tlcd_flagIF =0;
    lcd_cmd(0x80);
    
    while(_Tlcd_flagIF == 0){ }
    _Tlcd_flagIF =0;


}

//Esta funci�n mostrar� el mensaje que hay en ese
//momento en la Ventana_LCD en el LCD
void mostrarMensaje(){
    int i=0;
    PR_Timer_Flag=25;
    TMR_X=0;
    _Tlcd_flagIF =0;

    while(_Tlcd_flagIF == 0){ }
    lcd_cmd(0x80);
    _Tlcd_flagIF =0;
    while(_Tlcd_flagIF == 0){ }
    _Tlcd_flagIF =0;
    while( i < 16 ){
        //Escribir caracter
        lcd_data(Ventana_LCD[0][i]);
        //Espera de escritura
        while(_Tlcd_flagIF == 0){ }
        _Tlcd_flagIF =0;
        i++;
    }
    //Escribimos en la l�nea de abajo
    lcd_cmd(0xC0);
    while(_Tlcd_flagIF == 0){ }
    _Tlcd_flagIF =0;
    i=0;
    while( i < 16 ){
        //Escribir caracter
        lcd_data(Ventana_LCD[1][i]);
        //Espera de escritura
        while(_Tlcd_flagIF == 0){ }
        _Tlcd_flagIF =0;
        i++;
    }
}

//Copia el mensaje correspondiente a la RAM
void copMensaje(unsigned char* mensaje){
    int i,j;

    for(i=0; i<16;i++){
    Ventana_LCD [0][i]=mensaje[i];
    }
    i=16;
    for(j=0; j<16;i++,j++){
        Ventana_LCD [1][j]=mensaje[i];
    }

}

//Interrupci�n del timerx
//Enviar� caracter a caracter cada 1,3ms
void _ISR _T_LCD_Interrupt(void){

    //Maquina de estados para enviar los cada caracter
    switch(estado){
        //Envio del posicionamiento del puntero
        case 1:
            lcd_cmd(0x80);
            estado=2;
        break;
        //Envio de los primeros 16 caracteres
        case 2:
            lcd_data(Ventana_LCD [0][indice]);
            indice++;
            if(indice > 15){
                indice=0;
                estado=3;
            }
        break;
        //Envio del salto de l�nea
        case 3:
            lcd_cmd(0xC0);
            estado=4;
        break;
        //Envio al LCD de los �ltimos 16 caracteres
        case 4:
            lcd_data(Ventana_LCD [1][indice]);
            indice++;
            if(indice > 15){
                estado=1;
                indice=0;
            }
        break;
        default:
        break;
    }//Fin m�quina de estados

   _TlcdIF = 0;
}

