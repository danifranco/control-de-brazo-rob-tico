/*
 * Fichero: I2C.h
 *
 * Grupo 4	Autor Daniel Franco
 *
 *		v1.0	Fecha: 2014-III-18
 *
 *              v1.1    Fecha: Abril-2014
 */
 
#include "p24HJ256GP610A.h"

//This file contains the function prototypes for the i2c function
#define PAGESIZE	32

//Define el timer a utilizar
#define I2C_timer 7 
//Activar I2c -> 1, Desactivar -> 0
#define activar_I2C 0

//Low Level Functions
unsigned int IdleI2C_1(void);
unsigned int StartI2C_1(void);
unsigned int WriteI2C_1(unsigned char);
unsigned int StopI2C_1(void);
unsigned int RestartI2C_1(void);
unsigned int getsI2C_1(unsigned char*, unsigned char);
unsigned int NotAckI2C_1(void);
unsigned int InitI2C_1(void);
unsigned int ACKStatus_1(void);
unsigned int getI2C_1(void);
unsigned int AckI2C_1(void);
unsigned int EEAckPolling_1(unsigned char);
unsigned int putstringI2C_1(unsigned char*);

//High Level Functions for Low Density Devices
unsigned int LDByteReadI2C_1
    (unsigned char, unsigned char, unsigned char*, unsigned char);

unsigned int LDByteWriteI2C_1
    (unsigned char, unsigned char, unsigned char);

unsigned int LDPageWriteI2C_1
    (unsigned char, unsigned char, unsigned char*);

unsigned int LDSequentialReadI2C_1
     (unsigned char, unsigned char,  unsigned char*, unsigned char);

//High Level Functions for High Density Devices
unsigned int HDByteReadI2C_1
   (unsigned char, unsigned char, unsigned char, unsigned char*, unsigned char);

unsigned int HDByteWriteI2C_1
    (unsigned char, unsigned char, unsigned char, unsigned char);

unsigned int HDPageWriteI2C_1
    (unsigned char, unsigned char, unsigned char, unsigned char*);

unsigned int HDSequentialReadI2C_1
   (unsigned char, unsigned char, unsigned char, unsigned char*, unsigned char);

//Funciones añadidas


#if (I2C_timer == 1)
    #define _Ti2cIF _T1IF
#elif (I2C_timer == 2)
    #define _Ti2cIF _T2IF
#elif (I2C_timer == 3)
    #define _Ti2cIF _T3IF
#elif (I2C_timer == 4)
    #define _Ti2cIF _T4IF
#elif (I2C_timer == 5)
    #define _Ti2cIF _T5IF
#elif (I2C_timer == 6)
    #define _Ti2cIF _T6IF
#elif (I2C_timer == 7)
    #define _Ti2cIF _T7IF
#elif (I2C_timer == 8)
    #define _Ti2cIF _T8IF
#elif (I2C_timer == 9)
    #define _Ti2cIF _T9IF
#elif (I2C_timer == 0 || I2C_timer > 9)
    #error ("TIMER para I2C NO DEFINIDO")
#endif