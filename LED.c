/*
 * Fichero: LED.c
 *
 * Grupo 4	Autor Daniel Franco
 *
 *		v1.0	Fecha: 2014-III-18
 *
 *              v1.1    Fecha: Abril-2014
 */

#include "LED.h"

//Variables para el control de los LEDs
int sentido;
int led;

//Inicializa los leds
void Inic_Leds (void){
// Inicializa las patas de los leds de salida y el valor inicial
    _TRISA0 = 0;		
    _TRISA3 = 0;
    _TRISA6 = 0;
    _TRISA1 = 0;
    _TRISA2 = 0;
    _TRISA4 = 0;
    _TRISA5 = 0;
    _TRISA6 = 0;
    _TRISA7 = 0;

    _LATA0= 0;  
    _LATA1= 0;
    _LATA2= 0;
    _LATA3= 0;
    _LATA4= 0;
    _LATA5= 0;
    _LATA6= 0;
    _LATA7 = 0;
	
	sentido =0;
	led = 1;
	
	//Inicializacion timerx
	//Este timer se utilizar� para el tiempo de espera
	//de las luces intermitentes
	//=============================================
    #if (luces_timer == 1)
    _T1IF=0;
	_T1IE=0;
	PR1=10080;
	T1CON=0;
	T1CONbits.TON=1;
	//prescaler a 1:64
	T1CONbits.TCKPS0=0;
	T1CONbits.TCKPS1=1;
    #endif
	//=============================================
    #if (luces_timer == 2)
    _T2IF=0;
	_T2IE=0;
	PR2=10080;
	T2CON=0;
	T2CONbits.TON=1;
	//prescaler a 1:64
	T2CONbits.TCKPS0=0;
	T2CONbits.TCKPS1=1;
    #endif
	//=============================================
    #if (luces_timer == 3)
    _T3IF=0;
	_T3IE=0;
	PR3=10080;
	T3CON=0;
	T3CONbits.TON=1;
	//prescaler a 1:64
	T3CONbits.TCKPS0=0;
	T3CONbits.TCKPS1=1;
    #endif
	//=============================================
    #if (luces_timer == 4)
    _T4IF=0;
	_T4IE=0;
	PR4=10080;
	T4CON=0;
	T4CONbits.TON=1;
	//prescaler a 1:64
	T4CONbits.TCKPS0=0;
	T4CONbits.TCKPS1=1;
    #endif
	//=============================================
    #if (luces_timer == 5)
    _T5IF=0;
	_T5IE=0;
	PR5=10080;
	T5CON=0;
	T5CONbits.TON=1;
	//prescaler a 1:64
	T5CONbits.TCKPS0=0;
	T5CONbits.TCKPS1=1;
    #endif
	//=============================================
    #if (luces_timer == 6)
    _TlucesIF=0;
	_T6IE=0;
	PR6=10080;
	T6CON=0;
	T6CONbits.TON=1;
	//prescaler a 1:64
	T6CONbits.TCKPS0=0;
	T6CONbits.TCKPS1=1;
    #endif
	//=============================================
    #if (luces_timer == 7)
    _T7IF=0;
	_T7IE=0;
	PR7=10080;
	T7CON=0;
	T7CONbits.TON=1;
	//prescaler a 1:64
	T7CONbits.TCKPS0=0;
	T7CONbits.TCKPS1=1;
    #endif
	//=============================================
    #if (luces_timer == 8)
    _T8IF=0;
	_T8IE=0;
	PR8=10080;
	T8CON=0;
	T8CONbits.TON=1;
	//prescaler a 1:64
	T8CONbits.TCKPS0=0;
	T8CONbits.TCKPS1=1;
    #endif
	//=============================================
    #if (luces_timer == 9)
    _T9IF=0;
	_T9IE=0;
	PR9=10080;
	T9CON=0;
	T9CONbits.TON=1;
	//prescaler a 1:64
	T9CONbits.TCKPS0=0;
	T9CONbits.TCKPS1=1;
    #endif
}


//Funcion que mostrar� las luces intermitentes
void lucesIntermitentes(){
//Bucle para la visualizaci�n de los leds
    while(1){
     //M�quina de estados para mostrar en cada estado un led
     switch(led){
         case 1:
              _LATA1= 0;
              _LATA0= 1; // Encender LED_3
              //Bucle de espera
              while(_TlucesIF == 0){ }
              _TlucesIF = 0;
              if(salir){
                  break;
              }else{
                //Le damos un valor para que vaya al default
                led=2;
                sentido=0;
              }
         break;
         case 2:
             _LATA0= 0;
             _LATA1= 1; // Encender LED_4
             //Bucle de espera
              while(_TlucesIF == 0){ }
              _TlucesIF = 0;
              //Cambio de estado
              if(!sentido){
                led=3;
              }else if(salir){
                  break;
              }else{
                  led=1;
                  _LATA2= 0;
              }
         break;
         case 3:
             _LATA1= 0;
            _LATA2= 1; // Encender LED_5
            //Bucle de espera
            while(_TlucesIF == 0){ }
              _TlucesIF = 0;
              //Cambio de estado
              if(!sentido){
                led=4;
              }else if(salir){
                  break;
              }else{
                  led=2;
                  _LATA3= 0;
              }
         break;
         case 4:
             _LATA2= 0;
            _LATA3= 1; // Encender LED_6
            //Bucle de espera
              while(_TlucesIF == 0){ }
              _TlucesIF = 0;
              //Cambio de estado
              if(!sentido){
                led=5;
              }else if(salir){
                  break;
              }else{
                  led=3;
                  _LATA4= 0;
              }
         break;
         case 5:

             _LATA3= 0;
            _LATA4= 1; // Encender LED_7
            //Bucle de espera
             while(_TlucesIF == 0){ }
              _TlucesIF = 0;
              //Cambio de estado
              if(!sentido){
                led=6;
              }else if(salir){
                  break;
              }else{
                  led=4;
                  _LATA5= 0;
              }
         break;
         case 6:
             _LATA4= 0;
            _LATA5= 1; // Encender LED_8
            //Bucle de espera
              while(_TlucesIF == 0){ }
              _TlucesIF = 0;
              //Cambio de estado
              if(!sentido){
                led=7;
              }else if(salir){
                  break;
              }else{
                  led=5;
                  _LATA6= 0;
              }
         break;
         case 7:
             _LATA5= 0;
            _LATA6= 1; // Encender LED_9
            //Bucle de espera
              while(_TlucesIF == 0){ }
              _TlucesIF = 0;
              //Cambio de estado
              if(!sentido){
                led=8;
              }else if(salir){
                  break;
              }else{
                  led=6;
                  _LATA7= 0;
              }
         break;
         case 8:
             _LATA6= 0;
            _LATA7= 1; // Encender LED_10
            //Bucle de espera
              while(_TlucesIF == 0){ }
              _TlucesIF = 0;
              //Cambio de estado
              if(salir){
                  break;
              }else{
                led=7;
                sentido=1;
              }
         break;
         default:
             //Encenderemos todos los leds
             _LATA0= 1;
             _LATA1= 1;
             _LATA2= 1;
             _LATA3= 1;
             _LATA4= 1;
             _LATA5= 1;
             _LATA6= 1;
             _LATA7= 1;
         break;
     }//Fin m�quina de estados
     if(salir){
         //Volvemos al estado del menu
         pulsado=4;
         salir=0;
         dentro=0;
        break;
     }
 }//Fin while
}