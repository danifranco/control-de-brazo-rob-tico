/*
 * Fichero: main.c
 *
 * Grupo 4	Autor Daniel Franco
 *
 *		Fecha: 2014-III-18
 */

#include "p24HJ256GP610A.h" 
#include "LED.h"
#include "Oscilador.h"
#include "LCD.h"
#include "pulsador.h"
#include "RS232_2.h"
#include "PWM.h"
#include "I2C.h"

//Variable de control de los pulsadores
extern int pulsado;
extern int salir;

//Variable de control de los CADs
extern int joystick;

//Variable de control del UART
extern int escribir;

//Variable del sensor e I2C
unsigned char datoI2C[32];
extern unsigned char Ventana_LCD[2][16];
int val5,val6,val7,val8;

main(void)
{

// Inicializaciones de cada modulo
Inic_Oscilator();
Inic_Leds();
inicializar_lcd();
Inic_Pulsadores();
inicializarCAD();
initRS232_2();
inicializar_PWM();
InitI2C_1();

//Copiamos el mensaje del menu
copia_FLASH_RAM(1);
mostrarMensaje();

//Activamos la primera transmision
_U2TXIF = 1;


// main loop
while(1){
    //Esta m�quina de estados ser� el menu principal
    //La decisi�n del estado se basar� en la pulsaci�n de los pulsadores
    //Estas ser�m las opciones que tendr�:
    //      1. Luces Intermitentes  <--- Pulsando S3
    //      2. Cronometro           <--- Pulsando S4
    //      3. CA/D                 <--- Pulsando S6
	//			3.1 Potenciometro y sensor	  <--- Pulsando 
	//			3.2 Joystick			  <--- Pulsando 
    //      4. Men� Principal       <--- Resto de opciones
		
    switch(pulsado){
				
        //Luces intermitentes
        case 1:
            pulsado=0;
            //Copia el mensaje correspondiente del estado de las luces
            copia_FLASH_RAM(3);
            //Imprime el mensaje en el LCD
            mostrarMensaje();
            lucesIntermitentes();
            break;

        //Cronometro
        case 2:
            pulsado=0;
            //Copiamos el mensaje del cron�metro en la flash
            copia_FLASH_RAM(2);

            activarTimersCrono();
            break;

        //CA/D
        case 3:
            //Copiamos el mensaje correspondiente al menu del CAD
            copia_FLASH_RAM(6);
            mostrarMensaje();

            while(joystick == 0 && !salir){}
            if (salir) break;
            switch(joystick){
                //La opci�n elegida a sido la del potenciometro y el sensor
                case 1:
                    copia_FLASH_RAM(4);
                    mostrarMensaje();
                    break;

                //La opcion elegida a sido la del joystick
                case 2:
                    copia_FLASH_RAM(5);
                    mostrarMensaje();
                    break;
            }
            activarTimersCAD();
            while(!salir);
            descativarTimersCAD();
            joystick=0;
            break;

        //Estado en el que volveremos a mostrar el menu
        case 4:
            //Copiamos el mensaje del menu
            copia_FLASH_RAM(1);
            mostrarMensaje();
            //Pasaremos al estado default de la maquina de estados esperando
            //la acci�n del usuario
            pulsado=0;
            //Apagamos todos los LEDs
             _LATA0= 0;
             _LATA1= 0;
             _LATA2= 0;
             _LATA3= 0;
             _LATA4= 0;
             _LATA5= 0;
             _LATA6= 0;
             _LATA7= 0;
            break;
    }
    //Funcionalidad del sensor de distancia con el I2C
    if(_Ti2cIF==1){
        _Ti2cIF=0;
		
        LDByteReadI2C_1(0xE0,0,datoI2C, 4);
        LDByteWriteI2C_1(0xE0,0, 0x51);

        //Visualizacion del valor del sensor de luz
        val5 = (int)datoI2C[1]/1000;
        Ventana_LCD[0][6]=val5+0x30;
        val6 = (int)(datoI2C[1] -val5*1000)/100;
        Ventana_LCD[0][7]=val6+0x30;
        val7 = (int)(datoI2C[1] -val5*1000 -val6*100)/10;
        Ventana_LCD[0][8]=val7+0x30;
        val8 = (int) (datoI2C[1] -val5*1000 -val6*100 -val7*10);
        Ventana_LCD[0][9]=val8+0x30;

        //Visualizacion del valor del primer trozo del alcance
        val5 = (int)datoI2C[2]/1000;
        Ventana_LCD[1][6]=val5+0x30;
        val6 = (int)(datoI2C[2] -val5*1000)/100;
        Ventana_LCD[1][7]=val6+0x30;
        val7 = (int)(datoI2C[2] -val5*1000 -val6*100)/10;
        Ventana_LCD[1][8]=val7+0x30;
        val8 = (int) (datoI2C[2] -val5*1000 -val6*100 -val7*10);
        Ventana_LCD[1][9]=val8+0x30;

        //Visualizacion del valor del segundo trozo del alcance
        val5 = (int)datoI2C[3]/1000;
        Ventana_LCD[1][10]=val5+0x30;
        val6 = (int)(datoI2C[3] -val5*1000)/100;
        Ventana_LCD[1][11]=val6+0x30;
        val7 = (int)(datoI2C[3] -val5*1000 -val6*100)/10;
        Ventana_LCD[1][12]=val7+0x30;
        val8 = (int) (datoI2C[3] -val5*1000 -val6*100 -val7*10);
        Ventana_LCD[1][13]=val8+0x30;

        mostrarMensaje();
    }

    if(salir){
        salir=0;
        pulsado=4;
    }
    if(escribir){
        mostrarMensaje();
        escribir=0;
    }
}//Fin bucle principal

} //Fin main







