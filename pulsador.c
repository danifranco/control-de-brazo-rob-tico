/*
 * Fichero: pulsador.c
 *
 * Grupo 4	Autor Daniel Franco
 *
 *		v1.0	Fecha: 2014-III-18
 *
 *              v1.1    Fecha: Abril-2014
 */

#include "pulsador.h"

//Variables de control de los pulsadores
int pulsado;
int salir;

//Para el estado del cronometro
extern int cronoActivo;
//Se utilizará pàra determinar si está dentro de
//alguna funcion, es decir, crono, CA/D etc.
int dentro;

//Variable de control del estado del joystick
int joystick;

//Inicializa los pulsadores
void Inic_Pulsadores(void){
	_TRISD6 = 1; // Pata de Entrada (In= 1)
	_TRISD7= 1;
	_TRISD13 = 1;

	_CN15IE = 1; // Habilita interrupci�n asociada al Pulsador S3 (RD6)
	_CN16IE = 1; // Habilita interrupci�n asociada al Pulsador S6 (RD7)
	_CN19IE = 1; // Habilita interrupci�n asociada al Pulsador S4 (RD13)

	_CNIF = 0; // Borra flag int. CN, P4HJ256GP610A.h -> IFS1 -> #define _CNIF IFS1bits.CNIF
	_CNIE = 1; // Habilta int. global CN, P4HJ256GP610A.h -> IEC1 -> #define _CNIE IEC1bits.CNIE

	pulsado=0;
	salir=0;
	dentro=0;
	joystick=0;
}


// Gestiona los pulsadores por interrupci�n
void _ISR _CNInterrupt (void)
{
    //Pulsador S4 cuando estamos en el menu principal
    if(_RD13 == 0 && cronoActivo==0 && dentro==0){
        pulsado=2;
        dentro=1;
	//Pulsador S4 cuando estemos en le menu del CAD
    }else if(_RD13 == 0 && cronoActivo==0 && dentro==1){
        joystick=1;
    }
    //Pulsador S3 cuando estamos en el menu principal
    if(_RD6 == 0 && cronoActivo==0 && dentro==0){
        pulsado=1;
        dentro=1;
    }
    //Pulsador S3 cuando estemos en le menu del CAD
    else if(_RD6 == 0 && cronoActivo==0 && dentro==1){
            joystick=2;
    }
    //Pulsador S6 cuando esta activado el crono
    if(_RD7 == 0 && cronoActivo==1){
        resetearCrono();
        dentro=0;
        salir=1;
    }else if( _RD7 == 0 && cronoActivo==0 && dentro==1){
        salir=1;
        dentro=0;
    }else if(_RD7 == 0 && dentro==0 && cronoActivo==0){
        pulsado=3;
        dentro=1;
    }
 //Borra flag de interrupci�n general de CN
 _CNIF = 0; 
} 