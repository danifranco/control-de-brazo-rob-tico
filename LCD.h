/*
 * Fichero: LCD.h
 *
 * Grupo 4	Autor Daniel Franco
 *
 *		v1.0	Fecha: 2014-III-18
 *
 *              v1.1    Fecha: Abril-2014
 */

#include "p24hj256gp610A.h"

// Control signal data pins
#define RW _LATD5 // LCD R/W signal
#define RS _LATB15 // LCD RS signal
#define E _LATD4 // LCD E signal

// Control signal pin direction
#define RW_TRIS _TRISD5
#define RS_TRIS _TRISB15
#define E_TRIS _TRISD4

// Data signals and pin direction
#define DATA LATE // Port for LCD data
#define DATAPORT PORTE
#define TRISDATA TRISE // I/O setup for data Port

//Define el timer a utilizar para enviar los caracteres al LCD
#define lcd_timer 5

//Define el timer a utilizar para las esperas entre los caracteres enviados
#define lcd_timer_flag 4

//========= Env�o de COMANDO =========
void lcd_cmd( char cmd );

//======= Envio de DATO ===============
void lcd_data( char data );

//Copia el mensaje correspondiente a la ventana_LCD dependiendo
//del n�mero pasado por par�metro
void copia_FLASH_RAM(int decision);

//Inicializaci�n del LCD
void inicializar_lcd(void);

//Esta funci�n mostrar� el mensaje que hay en ese
//momento en la Ventana_LCD en el LCD
void mostrarMensaje();

//Copia el mensaje correspondiente a la RAM
void copMensaje(unsigned char* mensaje);

//Interrupci�n del timerx
//Enviar� caracter a caracter cada 1,3ms
void _ISR _T_LCD_Interrupt(void);

#if (lcd_timer == 1)
    #define _T_LCD_Interrupt _T1Interrupt
    #define _TlcdIF _T1IF
    #define _T1lcdIE _T1IE
#elif (lcd_timer == 2)
    #define _T_LCD_Interrupt _T2Interrupt
    #define _TlcdIF _T2IF
    #define _T1lcdIE _T2IE
#elif (lcd_timer == 3)
    #define _T_LCD_Interrupt _T3Interrupt
    #define _TlcdIF _T3IF
    #define _T1lcdIE _T3IE
#elif (lcd_timer == 4)
    #define _T_LCD_Interrupt _T4Interrupt
    #define _TlcdIF _T4IF
    #define _T1lcdIE _T4IE
#elif (lcd_timer == 5)
    #define _T_LCD_Interrupt _T5Interrupt
    #define _TlcdIF _T5IF
    #define _T1lcdIE _T5IE
#elif (lcd_timer == 6)
    #define _T_LCD_Interrupt _T6Interrupt
    #define _TlcdIF _T6IF
    #define _T1lcdIE _T6IE
#elif (lcd_timer == 7)
    #define _T_LCD_Interrupt _T7Interrupt
    #define _TlcdIF _T7IF
    #define _T1lcdIE _T7IE
#elif (lcd_timer == 8)
    #define _T_LCD_Interrupt _T8Interrupt
    #define _TlcdIF _T8IF
    #define _T1lcdIE _T8IE
#elif (lcd_timer == 9)
    #define _T_LCD_Interrupt _T9Interrupt
    #define _TlcdIF _T9IF
    #define _T1lcdIE _T1IE
#elif (lcd_timer == 0 || lcd_timer > 9)
    #error ("TIMER para LCD NO DEFINIDO")
#endif

#if (lcd_timer_flag == 1)
    #define _Tlcd_flagIF _T1IF
    #define PR_Timer_Flag PR1
    #define TMR_X TMR1
#elif (lcd_timer_flag == 2)
    #define _Tlcd_flagIF _T2IF
    #define PR_Timer_Flag PR2
    #define TMR_X TMR2
#elif (lcd_timer_flag == 3)
    #define _Tlcd_flagIF _T3IF
    #define PR_Timer_Flag PR3
    #define TMR_X TMR3
#elif (lcd_timer_flag == 4)
    #define _Tlcd_flagIF _T4IF
    #define PR_Timer_Flag PR4
    #define TMR_X TMR4
#elif (lcd_timer_flag == 5)
    #define _Tlcd_flagIF _T5IF
    #define PR_Timer_Flag PR5
    #define TMR_X TMR5
#elif (lcd_timer_flag == 6)
    #define _Tlcd_flagIF _T6IF
    #define PR_Timer_Flag PR6
    #define TMR_X TMR6
#elif (lcd_timer_flag == 7)
    #define _Tlcd_flagIF _T7IF
    #define PR_Timer_Flag PR7
    #define TMR_X TMR7
#elif (lcd_timer_flag == 8)
    #define _Tlcd_flagIF _T8IF
    #define PR_Timer_Flag PR8
    #define TMR_X TMR8
#elif (lcd_timer_flag == 9)
    #define _Tlcd_flagIF _T9IF
    #define PR_Timer_Flag PR9
    #define TMR_X TMR9
#elif (lcd_timer_flag == 0 || lcd_timer_flag > 9)
    #error ("TIMER_FLAG para LCD NO DEFINIDO")
#endif