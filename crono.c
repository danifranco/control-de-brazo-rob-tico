/*
 * Fichero: crono.c
 *
 * Grupo 4	Autor Daniel Franco
 *
 *		v1.0	Fecha: 2014-III-18
 *
 *              v1.1    Fecha: Abril-2014
 */

#include "crono.h"
#include "LCD.h"

//Variables del cronometro
int mili,decima,seg;
int cronoActivo;
int min,min1,min2;
int seg1,seg2,segAux;
int decima1,decima2;

//Inicializa el cron�metro
void inic_crono(){

    mili=0;
    decima=0;
    seg=0;
    cronoActivo=0;

    //=============================================
    #if ( crono_timer == 1)
            //Inicializacion timer1
            _T1IF=0;
            _T1IE=0;
            T1CON=0;
            PR1= 40000;
            T1CONbits.TON=1;
    #endif
    //=============================================
    #if ( crono_timer == 2)
            //Inicializacion timer2
            _T2IF=0;
            _T2IE=0;
            T2CON=0;
            PR2= 40000;
            T2CONbits.TON=1;
    #endif
    //=============================================
    #if ( crono_timer == 3)
            //Inicializacion timer1
            _T3IF=0;
            _T3IE=0;
            T3CON=0;
            PR3= 40000;
            T3CONbits.TON=1;
    #endif
    //=============================================
    #if ( crono_timer == 4)
            //Inicializacion timer4
            _T4IF=0;
            _T4IE=0;
            T4CON=0;
            PR4= 40000;
            T4CONbits.TON=1;
    #endif
    //=============================================
    #if ( crono_timer == 5)
            //Inicializacion timer5
            _T5IF=0;
            _T5IE=0;
            T5CON=0;
            PR5= 40000;
            T5CONbits.TON=1;
    #endif
    //=============================================
    #if ( crono_timer == 6)
            //Inicializacion timer6
            _T6IF=0;
            _T6IE=0;
            T6CON=0;
            PR6= 40000;
            T6CONbits.TON=1;
    #endif
    //=============================================
    #if ( crono_timer == 7)
            //Inicializacion timer7
            _T7IF=0;
            _T7IE=0;
            T7CON=0;
            PR7= 40000;
            T7CONbits.TON=1;
    #endif
    //=============================================
    #if ( crono_timer == 8)
            //Inicializacion timer8
            _T8IF=0;
            _T8IE=0;
            T8CON=0;
            PR8= 40000;
            T8CONbits.TON=1;
    #endif
    //=============================================
    #if ( crono_timer == 9)
            //Inicializacion timer9
            _T9IF=0;
            _T9IE=0;
            T9CON=0;
            PR9= 40000;
            T9CONbits.TON=1;
    #endif
}

//Interrupci�n del timer
//Contador que se utilizar� para la visualizaci�n del crono
void _ISR _T_Crono_Interrupt(void)
{
    if(mili<=99){
        mili++;
    }else{
        mili=0;
        mili++;
        if(decima <= 9){
            //Encendemos el LED correspondienten a las d�cimas
            _LATA5 = !_LATA5;
            decima++;
            //Calculamos los valores de la d�cimas de segundo
            decima1=(int)decima/10;
            decima2=decima - decima1*10;
            //Escribir las d�cimas en la ventana LCD
            Ventana_LCD[1][13]=decima1+0x30;
            Ventana_LCD[1][14]=decima2+0x30;
        }else{
            //Encendemos el led correspondiente a los segundos
            _LATA6 = !_LATA6;
            decima=0;
            seg++;
            //Calculamos los valores de los segundos
            segAux=seg - min*60;
            seg1=(int)segAux/10 ;
            seg2=segAux - seg1*10;
            //Escribir los segundos en la ventana LCD
            Ventana_LCD[1][10]=seg1+0x30;
            Ventana_LCD[1][11]=seg2+0x30;
        }
     }

     //Calculamos los valores de los minutos
     min= (int)(seg/60);
     min1=(int)min/10;
     min2=min - min1*10;
     //Escribir los minutos en la ventana del LCD
     Ventana_LCD [1][7]=min1+0x30;
     Ventana_LCD [1][8]=min2+0x30;

    _TcronoIF = 0; //Borra la interrupcion del timerX
}


//Activamos las interrupciones de los timers del crono
void activarTimersCrono(){
    _TcronoIF=1;
    _T1lcdIE=1;
    cronoActivo=1;
}

//Resetea los valores del cronometro y desactiva las interrupciones
void resetearCrono(){
    _TcronoIF=0;
    _T1lcdIE=0;
    mili=0;
    decima=0;
    seg=0;
    cronoActivo=0;
}

