 /*
 * Fichero: RS232_2.c
 *
 * Grupo 4	Autor Daniel Franco
 *
 *		v1.0	Fecha: 2014-III-18
 *
 *              v1.1    Fecha: Abril-2014
 */
 
#include "RS232_2.h"

#define Fosc 80000000 //Frecuencia reloj CPU
#define Fcy Fosc/2
#define BAUDRATE2 9600 // Velocidad de transmisión (Baudios)

#define BAUD_RATEREG_2_BRGH1 ((Fcy/BAUDRATE2)/4)-1 // Para BRGH = 1
#define BAUD_RATEREG_2_BRGH0 ((Fcy/BAUDRATE2)/16)-1 // Para BRGH = 0

extern unsigned char Ventana_LCD[2][16];

//Variables de control de la transmisión
int x=0;
int y=0;

//Variables de control de la recepcion
int z=0;
int k=0;

//Variables de control de la UART
int envio=0;
int escribir=0;

// Inicializa la UART2 
void initRS232_2() 
{
	_TRISF4=1;
	_TRISF5=0;
	U2MODE = 00; // 8bits, sin paridad, 1 stop, Uart parada
	 //Velocidad
	 U2MODEbits.BRGH=0;
	 U2STA = 00;
	 U2STAbits.URXISEL = 00; // Tipo interrupción en recepción -> 00 =  interrupción cuando buffer lleno
     U2BRG=BAUD_RATEREG_2_BRGH0;

	// Interrupción Envío
	 _U2TXIF = 0;
	 _U2TXIE = 1;

	// Interrupción Recepción
	 _U2RXIF = 0;
	 _U2RXIE = 1;
  
	 U2MODEbits.UARTEN=1; // Habilita UART
	 U2STAbits.UTXEN = 1; // Habilita Transmisión
	 U2TXREG=0;
} 

// ================ Servicio INTERRUPCION TRANSMISION RS232_2 ===============
// Trasmite un dato, si hay, al final de transmisión del anterior
void _ISR _U2TXInterrupt( void) 
{
    switch(envio){
        case 0:
            U2TXREG= Ventana_LCD[x][y];
            y++;
            if(y > 15){
              y=0;
              if(x==0)x=1;
              else{
                  x=0;
                  envio=1;
              }
            }         
            break;
        case 1:
            U2TXREG=CR;
            envio=2;
            break;
        case 2:
            U2TXREG=LF;
            envio=0;
            break;
    }
 _U2TXIF = 0; 
} 
 
 
// ================ Servicio INTERRUPCION Recepción RS232_2 ===============
void _ISR _U2RXInterrupt( void) 
{ 
 Ventana_LCD[z][k]= U2RXREG;
 k++;
 if( k > 15){
     k=0;
     if(z==0)z=1;
     else z=0;
 }
 escribir=1;
 _U2RXIF = 0; 
}  
 
 /*
// ================ Servicio INTERRUPCION Error RS232_2 =============== 
void _ISR _U2ErrInterrupt( void) 
{ 
 
 _U2ERIF = 0; 
} 
*/
 