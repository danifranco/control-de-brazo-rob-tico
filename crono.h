/*
 * Fichero: crono.h
 *
 * Grupo 4	Autor Daniel Franco
 *
 *		v1.0	Fecha: 2014-III-18
 *
 *              v1.1    Fecha: Abril-2014
 */

#include "p24HJ256GP610A.h"

//Define el timer a utilizar
#define crono_timer 1

//Variables extendidas
extern unsigned char Ventana_LCD[2][16];

//Inicializa el cronómetro
void inic_crono();

//Interrupción del timer
//Contador que se utilizará para la visualización del crono
void _ISR _T_Crono_Interrupt(void);

//Activamos las interrupciones de los timers del crono
void activarTimersCrono();

//Resetea los valores del cronometro y desactiva las interrupciones
void resetearCrono();

#if (crono_timer == 1)
    #define _T_Crono_Interrupt _T1Interrupt
    #define _TcronoIF _T1IF
#elif (crono_timer == 2)
    #define _T_Crono_Interrupt _T2Interrupt
    #define _TcronoIF _T2IF
#elif (crono_timer == 3)
    #define _T_Crono_Interrupt _T3Interrupt
    #define _TcronoIF _T3IF
#elif (crono_timer == 4)
    #define _T_Crono_Interrupt _T4Interrupt
    #define _TcronoIF _T4IF
#elif (crono_timer == 5)
    #define _T_Crono_Interrupt _T5Interrupt
    #define _TcronoIF _T5IF
#elif (crono_timer == 6)
    #define _T_Crono_Interrupt _T6Interrupt
    #define _TcronoIF _T6IF
#elif (crono_timer == 7)
    #define _T_Crono_Interrupt _T7Interrupt
    #define _TcronoIF _T7IF
#elif (crono_timer == 8)
    #define _T_Crono_Interrupt _T8Interrupt
    #define _TcronoIF _T8IF
#elif (crono_timer == 9)
    #define _T_Crono_Interrupt _T9Interrupt
    #define _TcronoIF _T9IF
#elif (crono_timer == 0 || crono_timer > 9)
    #error ("TIMER para CRONO NO DEFINIDO")
#endif
