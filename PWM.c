/*
 * Fichero: PWM.c
 *
 * Grupo 4	Autor Daniel Franco
 *
 *		v1.0	Fecha: 2014-III-18
 *
 *              v1.1    Fecha: Mayo-2014
 */

#include "p24HJ256GP610A.h"
#include <float.h>
#include "PWM.h"

//Variables del servo
int dormirBrazo; //Determina si el brazo se tiene que quedar en una posici�n de "dormido"
int esperaDormir; //Determina si se tiene que hacer una espera hasta que el brazo haya cogido la posici�n de dormir

//Variables de control del brazo
extern int controlTeclado; //Determina si se va a controlar por el teclado el brazo
extern int servo0,servo1,servo2,servo3,servo4; //Valores de cada servo
extern int s0,s1,s2,s3,s4; //Variables aux1_inciliares para guardar los valores de cada servo en un preciso momento
extern int servoX,servoY,valorPalanca,valorGatillo,valorPinza;

//Variables del inclin�metro
int inclinometroX,inclinometroY, inclinometroZ, inc_anterior, duty_anterior; //Valores de los ejes del inclin�metro
int contador; //Cuenta el n�mero de veces que se intenta cambiar el valor del 4� servo
//Variaables auxiliares
int aux1_inc,aux2_inc;

//Inicializaci�n del modulo de Salida Comparada(PWM)
void inicializar_PWM()
{
        //Inicializaci�n de variables
        TRIS_pwm5_soft=0; //Pata del servo por soft. de salida
        dormirBrazo=0;
        esperaDormir=1;
        inclinometroX=0;
        inclinometroY=0;
        inclinometroZ=0;
		inc_anterior= 0;
        duty_anterior=mitad_servo;
        aux1_inc=0;
        contador=0;
	//=============================================
	#if ( pwm_timer == 2)
		_T2IF=0;
		_T2IE=0;
		T2CON=0;
		PR2= 12500;
		T2CONbits.TON=1;
		//prescaler a 1:64
		T2CONbits.TCKPS0=0;
		T2CONbits.TCKPS1=1;
		
		OC1CON = 0; // Reset del m�dulo
		OC1CONbits.OCTSEL = 0; // Selecci�n del Timer 2 como timer de referencia

                OC2CON = 0; // Reset del m�dulo
		OC2CONbits.OCTSEL = 0; // Selecci�n del Timer 2 como timer de referencia

                OC3CON = 0; // Reset del m�dulo
		OC3CONbits.OCTSEL = 0; // Selecci�n del Timer 2 como timer de referencia

                OC4CON = 0; // Reset del m�dulo
		OC4CONbits.OCTSEL = 0; // Selecci�n del Timer 2 como timer de referencia

	#endif
	
	//=============================================
	#if ( pwm_timer == 3)
		_T3IF=0;
		_T3IE=0;
		T3CON=0;
		PR3= 12500;
		T3CONbits.TON=1;
		//prescaler a 1:64
		T3CONbits.TCKPS0=0;
		T3CONbits.TCKPS1=1;
		
		OC1CON = 0; // Reset del m�dulo
		OC1CONbits.OCTSEL = 1; // Selecci�n del Timer 3 como timer de referencia 
	#endif

        #if(pwm_timer_soft == 2)
            _T2IF=0;
            _T2IE=0;
            T2CON=0;
            T2CONbits.TON=1;
            //prescaler a 1:64
            T2CONbits.TCKPS0=0;
            T2CONbits.TCKPS1=1;
        #endif
        #if(pwm_timer_soft == 3)
            _T3IF=0;
            _T3IE=0;
            T3CON=0;
            T3CONbits.TON=1;
            //prescaler a 1:64
            T3CONbits.TCKPS0=0;
            T3CONbits.TCKPS1=1;
        #endif

        TRIS_pwm1 = 1;
        TRIS_pwm2 = 1;
        TRIS_pwm3 = 1;
        TRIS_pwm4 = 1;

	OC1R = 812; // Define el duty cycle para el primer pulso PWM
	OC1RS = 812; // Define el duty cycle para el segundo pulso PWM
	OC1CONbits.OCM = 0b110; // Selecciona modo PWM sin protecci�n

        OC2R = 812; // Define el duty cycle para el primer pulso PWM
	OC2RS = 812; // Define el duty cycle para el segundo pulso PWM
	OC2CONbits.OCM = 0b110; // Selecciona modo PWM sin protecci�n

        OC3R = 600; // Define el duty cycle para el primer pulso PWM
	OC3RS = 600; // Define el duty cycle para el segundo pulso PWM
	OC3CONbits.OCM = 0b110; // Selecciona modo PWM sin protecci�n

        OC4R = mitad_servo; // Define el duty cycle para el primer pulso PWM
	OC4RS = mitad_servo; // Define el duty cycle para el segundo pulso PWM
	OC4CONbits.OCM = 0b110; // Selecciona modo PWM sin protecci�n
}



//Interrupcion del timer asociado al PWM
void _ISR _T_PWM_Interrupt( void )
{
 //Si el brazo no se tiene que ir a una posici�n de "dormido"
 if(!dormirBrazo && controlTeclado != 1){
     //Se ajusta el valor del servo para que no se mueva en el centro
     if (servoX < 490 || servoX > 530) OC1RS = (( servoX - MIN_POT_EJEX ) * ratioX ) + MIN_SERVO;

     //Se ajusta el valor del servo para que no se mueva en el centro
     if (servoY < 465 || servoY > 509) OC2RS = (( servoY - MIN_POT_EJEY ) * ratioY ) + MIN_SERVO;

     OC3RS = (( valorPalanca - MIN_POT_EJEX ) * ratioPalanca ) + MIN_SERVO;  

     //Realizamos una cuenta para que cambie de duty cada 10 interrupciones
     //ya que si no no le dar�a tiempo al mecanismo del servo a posicionarse
     //y estar�a constantemente moviendose
     if( (contador % 10) == 0)
     {
     //Se calculan la inclinaci�n que tiene el inclin�metro
	 aux2_inc=mitad_inc_Y - inclinometroY;
     aux1_inc = relacion_servo_inc / 2;
     aux1_inc=aux1_inc*aux2_inc;
     //Se calcula el nuevo duty para que el servo quede recto
     if (duty_anterior + aux1_inc < MIN_SERVO) OC4RS= MIN_SERVO;
     else if(duty_anterior + aux1_inc > MAX_SERVO) OC4RS= MAX_SERVO;
     else OC4RS =duty_anterior + aux1_inc ;
     //Guardamos el nuevo duty para trabajar sobre ese en la siguiente interrupci�n
     duty_anterior = OC4RS;
      }
	  
     //PR del timer asociado al servo por software
     _Tpwm_PR=valorPinza;

     //Establecer el timer para controlar el 5� servo por software
     LAT_pwm5_soft=1;
     _Tpwm_TMR=0;
     _Tpwm_softIF=0;
     _Tpwm_softIE=1;
     contador++;
//Si el brazo se tiene que ir a una posici�n de dormido 
 }else if(dormirBrazo ){
	 OC1RS = (( 500 - MIN_POT_EJEX ) * ratioX ) + MIN_SERVO;
	 OC2RS = (( 800 - MIN_POT_EJEY ) * ratioY ) + MIN_SERVO;
	 OC3RS = (( 800 - MIN_POT_EJEX ) * ratioPalanca ) + MIN_SERVO;
	 OC4RS=725;
	 _Tpwm_TMR=0;
	 _Tpwm_softIF=0;
	 _Tpwm_softIE=0;
	 esperaDormir=0; 
		 
  //Si el control se hace desde el teclado cuando no tiene que irse a "dormir" el brazo
  }else if(!dormirBrazo && controlTeclado == 1){
    
	//Asignamos los valores de los servos controlados por el teclado
        OC1RS =servo0;
	OC2RS =servo1;
	OC3RS =servo2;
        OC4RS =servo3;
	//PR del timer asociado al servo por software
	_Tpwm_PR=servo4;

	//Establecer el timer para controlar el 5� servo por software
	LAT_pwm5_soft=1;
	_Tpwm_TMR=0;
	_Tpwm_softIF=0;
	_Tpwm_softIE=1;
	
	//Asignacion de los valores de los servos para transmitirlos
	s0=OC1RS;
	s1=OC2RS;
	s2=OC3RS;
	s3=OC4RS;
	s4=_Tpwm_PR;
 }
 
 _TpwmIF = 0; // Borra flag interrupci�n
}

//Interrupcion del timer asociado al PWM
void _ISR _T_PWM_soft_Interrupt( void ){
    LAT_pwm5_soft=0;
    _Tpwm_softIE=0;
    _Tpwm_softIF=0;
}

